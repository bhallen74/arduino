
#ifndef TEST_EXECUTIVE_H
#define TEST_EXECUTIVE_H

#include "Arduino.h"

#define TEST_EXECUTIVE_VERSION "2.0"

// Defines the command definition table, don't touch this.
typedef struct cmdtype_t {
  char name[20];    // Name of test, "POWER_RELAY_ON", no spaces
  char desc[100];   // Description, "Turns on AC power to UUT"
  char arg[60];     // Description of argurments, can be "" or "1 turns on, 0 turns off".
  int (*func)(const String& args, struct cmdtype_t *tableEntry);  // Name of function to run to execute test, power_on
} cmdtype_t;

class TestExecutive
{

public:
    TestExecutive();
    void begin(const cmdtype_t * commandTable, Stream * serialPort);
    void run();
    void processCommand(String &cmd);
    void print(const __FlashStringHelper *fmt, ... );
    void print(const char *fmt, ... );
    int print_help();

private:
    Stream        * m_serialPort;
    cmdtype_t     * m_commandTable;
    String          m_cmd;
    bool            m_initialized;

    bool readSerialLine();
};


#endif
