
// Enercon test executive
//
//
// This module assumes that test interface uses the primary serial port (m_serialPort object)
// It also assumes that cmdTable has been defined and it is of cmdtype_t.
// When a command string has been recieved by this module, it will assign the command string to the cmd varible, making it non-zero.
// The main loop should pick this up and presumably call processCommand(cmd); which will parse and execute the command. When
// done, the cmd variable should be cleared, cmd="";.

#include "TestExecutive.h"

TestExecutive::TestExecutive()
{
    //m_serialPort=NULL;
    m_commandTable=NULL;
    m_cmd="";
    m_initialized=false;
    m_serialPort=NULL;
}

void TestExecutive::begin(const cmdtype_t * commandTable, Stream * serialPort)
{
    m_commandTable=(cmdtype_t *)commandTable;
    m_serialPort=serialPort;
    m_initialized=true;
    print(F("Welcome to Test Executive version %s\n"), TEST_EXECUTIVE_VERSION );
}

void TestExecutive::run()
{
    if(m_initialized && readSerialLine())
    {
        processCommand(m_cmd);
        m_cmd="";
    }
}

// Builds a string and passes it on to the command interpreter when a cr/lf is received
bool TestExecutive::readSerialLine()
{
  
    while (m_serialPort->available()) 
    {
        // get the new byte:
        char inChar = (char)m_serialPort->read();

        // add it to the inputString:
        if ((inChar != '\n') && (inChar != '\r') )
        {
            m_cmd += inChar;
        }

        // terminate on a newline or cr
        if ((inChar == '\n') || (inChar == '\r') )
        {
            if(m_cmd.length())
            {
                return true;
            }
        }
    }
    return false;
}

// Process the command entry.  Searches the command table for a match
// then executes it.
void TestExecutive::processCommand(String &cmd)
{
    int x=0;
    static cmdtype_t tableEntry;

    if(!m_initialized)
    {
        return;
    }

    // Loop through the command table to find a match
    do 
    {
        // Copy the table entry from flash into ram for easier manipulation
        memcpy_P((void *)&tableEntry, (const void *) &m_commandTable[x], sizeof(tableEntry));
        int len=strlen(tableEntry.name);

        x++; // Increment index here, will create bug with continue below if done at the end

        // If the name is blank, we're at the end of the table, break the loop.
        if(0 == len)
        {
            break;
        }

        // If the name matches
        if(cmd.startsWith(tableEntry.name))
        {
            
            // If there are arguements, make sure next char is space
            if(cmd.length() > len)
            {
                if(' ' != cmd.charAt(len) )
                {
                    // Otherwise the command is ambiguous, keep searching
                    continue;
                }
            }
            
            // Look for arguments
            String args = ""; // No arguments by default
            if(cmd.length() >= (len+2)) // +2 is space plus 1 char for argument (minimum)
            {
                args = cmd.substring(len+1);	// base zero index, so +1 this time
            }
            
            // execute the test function
            if( tableEntry.func(args, &tableEntry) )
            {
                // error message on non-zero return value
                print(F("ERROR executing command: %s\n"), cmd.c_str());
            }
            else
            {
                // execution OK
                print(F("OK\n"));
            }
            
            return;  // command matched, exit function
        }

    } while(1);

    if(cmd.startsWith("HELP"))
    {
        print_help();
        print(F("OK\n"));
        return;
    }

    // if we got here, command not found
    print(F("ERROR command not found: %s\n"), cmd.c_str());

}

// printf, const char * copied from flash to ram.
void TestExecutive::print(const char *fmt, ... )
{
    char buf[128]; // resulting string limited to 128 chars
    va_list args;

    if(NULL==m_serialPort)
    {
        return;
    }

    m_serialPort->flush(); // Wait for buffer to empty.  Tends to get clobbered
    va_start (args, fmt );
    vsnprintf(buf, 128, fmt, args);
    va_end (args);
    m_serialPort->print(buf);
}

// printf using const * from flash
void TestExecutive::print(const __FlashStringHelper *fmt, ... )
{
    char buf[128]; // resulting string limited to 128 chars
    va_list args;

    if(NULL==m_serialPort)
    {
        return;
    }

    m_serialPort->flush(); // Wait for buffer to empty.  Tends to get clobbered
    va_start (args, fmt);
#ifdef __AVR__
    vsnprintf_P(buf, sizeof(buf), (const char *)fmt, args); // progmem for AVR
#else
    vsnprintf(buf, sizeof(buf), (const char *)fmt, args); // for the rest of the world
#endif
    va_end(args);
    m_serialPort->print(buf);
}


// Show the help screen
int TestExecutive::print_help()
{
    int index=0;
    cmdtype_t tableEntry;

    if(!m_initialized)
    {
        return 1;
    }

    while (1) 
    {
        // Copy table entry from flash to ram
        memcpy_P((void *)&tableEntry, (const void *) &m_commandTable[index], sizeof(tableEntry));
        int len=strlen(tableEntry.name);

        if(0==len)  // If it's the last table entry, we're done.
        {
            break;
        }

        // print the entry help info
        print(F("    %s - %s\n"), tableEntry.name, tableEntry.desc);
        if(tableEntry.arg[0])
        {
            print(F("      arg: %s\n"), tableEntry.arg);
        }
        
        index++;  // Increment the index for next table entry
    }

    return 0;
}


/*
 * Pulls in a full line from the serial stream, or returns a null string if the buffer is empty.
 */
 /*
String readLine() 
{
  if (m_serialPort.available() <= 0)
    return "";

  //  Build the line one character at a time
  char charRead = m_serialPort.read();
  String stringBuild = "";
  while (charRead != '\n' && charRead != '\r') {
    // append the last character read
    stringBuild += charRead;

    // wait for a new character
    while (m_serialPort.available() <= 0) 
    {
      // TODO: Maybe add a timeout here?
    }    
    // read the new character
    charRead = m_serialPort.read();
  }

  return stringBuild;
}
*/