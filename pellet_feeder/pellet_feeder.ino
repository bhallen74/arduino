#include "ESP8266WiFi.h"

// https://github.com/nodemcu/nodemcu-devkit-v1.0
// https://github.com/esp8266/Arduino/tree/2.4.2/doc

/* Pinout
 * 1 - A0 - ADC0 - Divide by (220k/100k)
 * 2 - Reserved (ADC Direct?)
 * 3 - Reserved
 * 4 - D12 - GPIO10
 * 5 - D11 - GPIO9
 * 6 - INT - Interrupt?
 * 7 - MOSI
 * 8 - MISO
 * 9 - SCLK
 * 10 - GND
 * 11 - 3V3
 * 12 - EN
 * 13 - RST
 * 14 - GND
 * 15 - 5V
 * 
 * 16 - 3V3
 * 17 - GND
 * 18 - D10 - GPIO1 - USB TX0
 * 19 - D9 - GPIO3 - USB RX0
 * 20 - D8 - GPIO15 - Must be low on boot
 * 21 - D7 - GPIO13
 * 22 - D6 - GPIO12
 * 23 - D5 - GPIO14
 * 24 - GND
 * 25 - 3V3
 * 26 - D4 - GPIO2 - Must be high on boot
 * 27 - D3 - GPIO0 - Flash - High->Run, Low->Flash
 * 28 - D2 - GPIO4
 * 29 - D1 - GPIO5
 * 30 - D0 - GPIO16 - USER - Blue LED on Low
 */

// A0 - current sense
// 5V, Gnd - power in

#include "TestExecutive.h"

#define D0   16 // special - LED heartbeat
#define D1   5  // level sense 1
#define D2   4  // level sense 2
#define D3   0  // special - flash key - ext button 1
#define D4   2  // special - high on boot
#define D5   14 // push button
#define D6   12 // led indicator 1 - state
#define D7   13 // led indicator 2 - error
#define D8   15 // special - low on boot
#define D9   3  // special - USB serial
#define D10  1  // special - USB serial
#define D11  9  // rotation detect
#define D12  10  // drive enable

#define LED_HEARTBEAT     D0
#define LEVEL_SENSE_1     D1
#define LEVEL_SENSE_2     D2
#define EXT_BUTTON_1      D3
#define EXT_BUTTON_2      D5
#define LED_1_ACTIVE      D0 //D6
#define LED_2_ERROR       D7
#define ROT_DETECT        D11
#define DRIVE_ENABLE      D12

#define ERROR_CODE_1_EMPTY        0x1
#define ERROR_CODE_2_NODEMAND     0x2
#define ERROR_CODE_3_NOROTATION   0x4
#define ERROR_CODE_3_NOWIFI       0x8

#define SENSOR_FULL       0
#define SENSOR_CLEAR      1
#define DRIVE_ON          1
#define DRIVE_OFF         0
#define LED_ON            0
#define LED_OFF           1

#define WAIT_NORMAL       (8 * 60 * 60 * 1000)   // 8 Hours
#define WAIT_BRIEF        (10 * 60 * 1000)       // 10 Minutes

#define TIME_RUN          (30 * 1000)     // 30 Sec
#define TIME_RUN_PAUSE    (30 * 1000)
#define TIME_NO_USAGE     (2 * 24 * 60 * 60 * 1000) // 2 Days
#define MAX_RUN_CYCLES    3

#define SENSOR_CLEAR_TIME   (10 * 1000) // 10 seconds
#define SENSOR_BLOCK_TIME   (1 * 1000)  // 1 second

enum states_e 
{
    STATE_RESET,
    STATE_RUN,
    STATE_RUN_PAUSE,
    STATE_IDLE,
    STATE_FAULT,
};

enum sensorState_e
{
    SENSORSTATE_BLOCKED,       // Sensor is blocked by pellets for >=1 second continuous
    SENSORSTATE_EMPTY,         // Sensor is empty (no pellets) for >=10 seconds
    SENSORSTATE_TRANSITION,    // Sensor actively sees pellets passing by
};

sensorState_e sensorState;
sensorState_e sensorState1=SENSORSTATE_BLOCKED;  // safest initial state
sensorState_e sensorState2=SENSORSTATE_BLOCKED;
states_e state=STATE_RESET;

const PROGMEM cmdtype_t cmdTable[] = {
  { "S",  "Status",                        "",                           show_status },
  { "", "", "", 0}, // Must be last entry
};

TestExecutive te;

uint32_t waitStart, waitTime;   // ~49 day overflow
uint32_t pauseStart;
uint8_t pauseCount;
uint32_t runCounter=0;
uint32_t transitionCount1, transitionCount2;
uint8_t lastTransition1, lastTransition2;
uint8_t errorCode=0;

void setup() 
{
    pinMode(LED_HEARTBEAT, OUTPUT);
    digitalWrite(LED_HEARTBEAT, LED_OFF);
    pinMode(LED_1_ACTIVE, OUTPUT);
    digitalWrite(LED_1_ACTIVE, LED_OFF);
    pinMode(LED_2_ERROR, OUTPUT);
    digitalWrite(LED_2_ERROR, LED_OFF);
    pinMode(DRIVE_ENABLE, OUTPUT);
    digitalWrite(DRIVE_ENABLE, DRIVE_OFF);

    pinMode(LEVEL_SENSE_1, INPUT);
    pinMode(LEVEL_SENSE_2, INPUT);
    pinMode(EXT_BUTTON_1, INPUT);   // INPUT_PULLUP or INPUT_PULLDOWN_16
    pinMode(EXT_BUTTON_2, INPUT);
    pinMode(ROT_DETECT, INPUT);

    Serial.begin(74880);
    Serial.println("Welcome to the pellet driver\n");

    lastTransition1=digitalRead(LEVEL_SENSE_1);
    lastTransition2=digitalRead(LEVEL_SENSE_2);
    transitionCount1=millis();
    transitionCount2=transitionCount1;

    //TODO: Show current state (function)
    //TODO: Show state on serial port input
    //TODO: LED state (green)
    //TODO: x LED error 
    //TODO: Heartbeat
    //TODO: Button inputs
}

int show_status(const String& args, struct cmdtype_t * cmd)
{

    return 0;
}

sensorState_e getSensorState()
{
    uint8_t currentSensor1=digitalRead(LEVEL_SENSE_1);
    uint8_t currentSensor2=digitalRead(LEVEL_SENSE_2);

    // Any change puts it into transition
    if(currentSensor1!=lastTransition1)
    {
        transitionCount1=millis();
        lastTransition1=currentSensor1;
        sensorState1=SENSORSTATE_TRANSITION;
    }

    // Check if there have been no changes for a predetermined time
    if(SENSORSTATE_TRANSITION==sensorState1)
    {
        uint32_t diff=(uint32_t)(millis()-transitionCount1);

        if( (SENSOR_CLEAR==currentSensor1) && ( diff > SENSOR_CLEAR_TIME) )
        {
            sensorState1=SENSORSTATE_EMPTY;
        }
        if( (SENSOR_FULL==currentSensor1) && ( diff > SENSOR_BLOCK_TIME) )
        {
            sensorState1=SENSORSTATE_BLOCKED;
        }
    }
        
    // Any change puts it into transition
    if(currentSensor2!=lastTransition2)
    {
        transitionCount2=millis();
        lastTransition2=currentSensor2;
        sensorState2=SENSORSTATE_TRANSITION;
    }

    // Check if there have been no changes for a predetermined time
    if(SENSORSTATE_TRANSITION==sensorState2)
    {
        uint32_t diff=(uint32_t)(millis()-transitionCount2);

        if( (SENSOR_CLEAR==currentSensor2) && ( diff > SENSOR_CLEAR_TIME) )
        {
            sensorState2=SENSORSTATE_EMPTY;
        }
        if( (SENSOR_FULL==currentSensor2) && ( diff > SENSOR_BLOCK_TIME) )
        {
            sensorState2=SENSORSTATE_BLOCKED;
        }
    }

    // If either is blocked, then that is the state
    if( (SENSORSTATE_BLOCKED==sensorState1) || (SENSORSTATE_BLOCKED==sensorState2) )
    {
        Serial.println("Sensor now blocked");
        digitalWrite(LED_1_ACTIVE, LED_OFF);
        return SENSORSTATE_BLOCKED;
    }

    // Both need to be clear for empty state
    if( (SENSORSTATE_EMPTY==sensorState1) && (SENSORSTATE_EMPTY==sensorState2) )
    {
        Serial.println("Sensor now empty");
        digitalWrite(LED_1_ACTIVE, LED_ON);
        return SENSORSTATE_EMPTY;
    }

    // Otherwise it's intdeterminate.
    return SENSORSTATE_TRANSITION;

}

void processDriveState()
{
    switch(state)
    {
        case STATE_RESET:
        {
            Serial.println("Resetting");

            digitalWrite(DRIVE_ENABLE, DRIVE_OFF);

            waitTime=WAIT_NORMAL;
            if(SENSORSTATE_EMPTY==sensorState)
            {
                waitTime=WAIT_BRIEF;
            }
            waitStart=millis();
            Serial.print("Waiting for ");
            Serial.println(waitTime);
            state=STATE_IDLE;
            break;
        }

        case STATE_RUN:
        {
            if(SENSORSTATE_BLOCKED==sensorState)
            {
                digitalWrite(DRIVE_ENABLE, DRIVE_OFF);
                state=STATE_RESET;
                Serial.println("Sensor blocked");
                break;
            }
            if( (millis()-runCounter) > TIME_RUN)
            {
                digitalWrite(DRIVE_ENABLE, DRIVE_OFF);
                pauseStart=millis();
                pauseCount++;
                if(pauseCount>=MAX_RUN_CYCLES)
                {
                    errorCode|=ERROR_CODE_1_EMPTY;
                    state=STATE_FAULT;  // Overrun
                }
                else
                {
                    state=STATE_RUN_PAUSE;
                }
                Serial.print("Drive timed out, count,state: ");
                Serial.print(pauseCount);
                Serial.println(state);
            }
            break;
        }

        case STATE_RUN_PAUSE:
        {
            if( (uint32_t)((millis()-pauseStart)) > TIME_RUN_PAUSE)
            {
                if(SENSORSTATE_EMPTY==sensorState)
                {
                    runCounter=millis();
                    digitalWrite(DRIVE_ENABLE, DRIVE_ON);
                    state=STATE_RUN;
                    Serial.println("Starting run after pause");
                }
                else
                {
                    state=STATE_RESET;
                    Serial.println("Sensor full after pause");
                }
            }
            break;
        }

        case STATE_IDLE:
        {
            if( (uint32_t)((millis()-waitStart)) > waitTime)
            {
                if(SENSORSTATE_EMPTY==sensorState)
                {
                    pauseCount=0;
                    errorCode&=(ERROR_CODE_2_NODEMAND^0xFF);
                    runCounter=millis();
                    digitalWrite(DRIVE_ENABLE, DRIVE_ON);
                    state=STATE_RUN;
                    Serial.println("Turning drive on..");
                }
            
            }
            if( (uint32_t)((millis()-waitStart)) > TIME_NO_USAGE)
            {
                // warn no usage -- not really an error
                errorCode|=ERROR_CODE_2_NODEMAND;
            }
            break;
        }

        case STATE_FAULT:
        {
            // digitalWrite(LED_2_ERROR, LED_ON);
            Serial.print("Fault state entered, code: ");
            Serial.println(errorCode);
            break;
        }

        default:
        {
            state=STATE_RESET;
            break;
        }
    }
}

#define LED_ON_TIME     500
#define LED_OFF_TIME    LED_ON_TIME
#define LED_GAP_TIME    1000

void LED_cycle()
{
    static uint32_t led_count;
    static uint32_t led_pos;
    static uint32_t led_blink_count;
    static int led_state=0;

    if(0==errorCode)
    {
        digitalWrite(LED_2_ERROR, LED_OFF);
        return;    
    }

    switch(led_state)
    {
        case 0:     //reset
        {
            led_pos=0;
            led_count=millis();
            led_state=1;
            digitalWrite(LED_2_ERROR, LED_OFF);
            break;
        }
        case 1:     // find bit position
        {
            for(int x=led_pos; x<8; x++)
            {
                if(errorCode & (1 << x))
                {
                    led_pos=x;
                    led_state=2;
                    led_blink_count=0;
                    digitalWrite(LED_2_ERROR, LED_ON);
                    led_count=millis();
                    break;
                }
            }
            if(8=x)
            {
                led_state=0;
            }
            break;
        }
        case 2:     // hold led on
        {
            if(led_blink_count > led_pos)
            {
                led_state=4;
            }
            if( (uint32_t)((millis()-led_count)) > LED_ON_TIME)
            {
                digitalWrite(LED_2_ERROR, LED_OFF);
                led_count=millis();
                led_state=3;
                led_blink_count++;
            }
            break;
        }
        case 3:     // hold led off
        {
            if( (uint32_t)((millis()-led_count)) > LED_OFF_TIME)
            {
                digitalWrite(LED_2_ERROR, LED_ON);
                led_count=millis();
                led_state=2;
            }
            break;
        }
        case 4:     // hold off, gap time between codes
        {
            if( (uint32_t)((millis()-led_count)) > LED_GAP_TIME)
            {
                digitalWrite(LED_2_ERROR, LED_OFF);
                led_count=millis();
                led_state=1;
            }
            break;
        }
        default:
        {
            led_state=0;
            break;
        }
    }
    
}

void loop() 
{
    te.run();
    sensorState=getSensorState();
    processDriveState();
    LED_cycle();
}
