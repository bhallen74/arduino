
// https://github.com/nodemcu/nodemcu-devkit-v1.0

// A0 - current sense
// 5V, Gnd - power in

static const uint8_t D0   = 16; // special - LED
static const uint8_t D1   = 5;  // level sense 1
static const uint8_t D2   = 4;  // level sense 2
static const uint8_t D3   = 0;  // special - flash key
static const uint8_t D4   = 2;  // rotation detect
static const uint8_t D5   = 14; // push button
static const uint8_t D6   = 12; // led indicator 1
static const uint8_t D7   = 13; // led indicator 2
static const uint8_t D8   = 15; // drive enable
static const uint8_t D9   = 3;  // serial
static const uint8_t D10  = 1;  // serial

#define LEVEL_SENSE_1     D1
#define LEVEL_SENSE_2     D2
#define EXT_BUTTON        D3
#define LED_1             D0
#define DRIVE_ENABLE      D8

#define SENSOR_FULL       0
#define SENSOR_CLEAR      1
#define DRIVE_ON          1
#define DRIVE_OFF         0

#define WAIT_NORMAL       (8 * 60 * 60)
#define WAIT_BRIEF        (10 * 60)

#define TIME_RUN          (30 * 1000)
#define TIME_RUN_PAUSE    (30 * 1000)
#define MAX_RUN_CYCLES    3

void setup() 
{
  pinMode(LED_1, OUTPUT);
  digitalWrite(LED_1, HIGH);
  pinMode(DRIVE_ENABLE, OUTPUT);
  digitalWrite(DRIVE_ENABLE, LOW);

  Serial.begin(74880);
  Serial.print("Welcome to the pellet driver\n\n");

}

enum states_e 
{
  STATE_RESET,
  STATE_RUN,
  STATE_RUN_PAUSE,
  STATE_IDLE,
  STATE_FAULT,
}

enum sensorstate_e
{
  SENSOR_BLOCKED,
  SENSOR_CLEAR,
  SENSOR_TRANSITION,
}

sensorstate_e sensorstate;
state_e state;
uint32_t waitStart;

sensorstate_e getSensorState()
{
  return SENSOR_BLOCKED;
}

void loop() 
{
  sensorstate=getSensorState();

  switch(state)
  {
    {
    case STATE_RESET:
      pinMode(LED_1, OUTPUT);
      digitalWrite(LED_1, HIGH);
      pinMode(DRIVE_ENABLE, OUTPUT);
      digitalWrite(DRIVE_ENABLE, LOW);
      waitTime=WAIT_NORMAL;
      if( (SENSOR_CLEAR==digitalRead(LEVEL_SENSE_1)) && (SENSOR_CLEAR==digitalRead(LEVEL_SENSE_2)) )
      {
        waitTime=WAIT_BRIEF;
      }
      waitStart=millis();
      state=STATE_IDLE;
      break;
    }

    case STATE_RUN:
    {
      if( ( (millis()-runCounter) > TIME_RUN) && (pauseCount < MAX_RUN_CYCLES) )
      {
        digitalWrite(DRIVE_ENABLE, LOW);
        pauseCounter=millis();
        pauseCount++;
        state=STATE_RUN_PAUSE;
      }
      else
      {
        digitalWrite(DRIVE_ENABLE, LOW);
        state=STATE_FAULT;
      }
      break;
    }

    case STATE_RUN_PAUSE:
    {

      break;
    }

    case STATE_IDLE:
    {
      if( (millis()-waitStart) > waitTime)
      {
        if( (SENSOR_CLEAR==digitalRead(LEVEL_SENSE_1)) && (SENSOR_CLEAR==digitalRead(LEVEL_SENSE_2)) )
        {
          pauseCount=0;
          runCounter=millis();
          digitalWrite(DRIVE_ENABLE, HIGH);
          state=STATE_RUN;
        }
        
      }
      break;
    }

    case STATE_FAULT:
    {

      break;
    }

    default:
      state=STATE_RESET;
      break;
  }
}
